Guidelines
When writing legal content, generally follow the style points outlined in the Voice and tone and Grammar and mechanics sections. Here are some more general considerations, too.

Start with the facts
We have some standard language that we use for common issues or requests, but since legal content is so fact-specific, we start there before getting into structure and format. That’s why you won’t see many templates for our legal content. Given the nature of our offerings, we try to provide three sources for facts:

1. Omnibus
2. Technical
3. Legal
4. tl;dr

Omnibus combines both technical and legal, our workflow for documentation works from a "trickle down git workflow" system. Documentation is seperated from the omnibus into the technical and legal. Then that is synthenzed into the fourth category, took long; didn't read.

Use plain language
Legal content is serious business, so the tone is slightly more formal than most of our content. That said, we want all of our users to be able to understand our legal content. So whenever possible, we use plain language rather than legal jargon.

Instead of: “If an individual purports, and has the legal authority, to sign these Terms of Use electronically on behalf of an employer or client then that individual represents and warrants that they have full authority to bind the entity herein to the terms of this hereof agreement”

We say: “If you sign up on behalf of a company or other entity, you represent and warrant that you have the authority to accept these Terms on their behalf.”

There are some legal terms we have to include because either there’s not a sufficient plain language alternative, or case law or statute dictates that term has to be used for the contract to hold up in court. For example, sometimes we need to say “represent and warrant” instead of “confirm” or “agree.” If we use those terms, we can provide an example or quick definition to help people understand what they’re reading. We can't avoid all legal terminology, but we can pare it down to what's necessary and sufficient. We provide additional case law references for jurisdictional issues arising in countries like the United States of America where State law and Federal law have divergences and State to State. 

Some companies have complicated terms and write plain-language summaries so people can understand the agreement. We don’t summarize our legal content, but instead try to write the terms themselves in plain language. We use an omnibus style of documentation to provide an in-depth resource that aims to provide examples or links to further reading for people who want more context.

Definitions

We use plain language for the terms we define in our omnibus documentation and legal documents. It makes it easier to read, and we are sure you probably have read contracts that say something like “The Corporation” or “The User” throughout, instead of “we” (meaning the company) and “you” (meaning the user who is agreeing to the terms). Where we can we place at the beginning of the document defining terms or provide an appendix for reference.

Contractions

While  using a contracting word doesn't affect the validity of an agreement, we do not use contractions in any legal text intentionally. We have used contractions in this document, specifically in the comment discussing "tl;dr" to illustrate the distinction. 

Never offer legal advice
While we want to inform our users about legal issues related to their use of the Freight Trust Networkp, we can’t offer them legal advice. The legal department will check for this in their content review typically. 
